# baobab ja.po.
# Copyright (C) 1998-2019 gnome-util's COPYRIGHT HOLDER
# Eiichiro ITANI <emu@ceres.dti.ne.jp>, 1998.
# Yuusuke Tahara <tahara@gnome.gr.jp>, 2000.
# Yukihiro Nakai <nakai@gnome.gr.jp>, 2000.
# Takayuki KUSANO <AE5T-KSN@asahi-net.or.jp>, 2000-2002, 2009, 2011.
# Sun G11n <gnome_int_l10n@ireland.sun.com>, 2002.
# KAMAGASAKO Masatoshi <emerald@gnome.gr.jp>, 2003.
# Takeshi AIHANA <takeshi.aihana@gmail.com>, 2003-2009.
# Satoru SATOH <ss@gnome.gr.jp>, 2006.
# Hideki Yamane (Debian-JP) <henrich@debian.or.jp>, 2009, 2010.
# Yasumichi Akahoshi <yasumichi@vinelinux.org>, 2010, 2011.
# Takayoshi OKANO <kano@na.rim.or.jp>, 2011.
# Shuuji Takahashi <shuuji3@gmail.com>, 2012.
# Noriko Mizumoto <noriko@fedoraproject.org>, 2012.
# Jiro Matsuzawa <jmatsuzawa@gnome.org>, 2012, 2013.
# sicklylife <translation@sicklylife.jp>, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: baobab master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/baobab/issues\n"
"POT-Creation-Date: 2019-03-24 19:41+0000\n"
"PO-Revision-Date: 2019-09-15 20:00+0900\n"
"Last-Translator: sicklylife <translation@sicklylife.jp>\n"
"Language-Team: Japanese <gnome-translation@gnome.gr.jp>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: data/org.gnome.baobab.appdata.xml.in:6 data/org.gnome.baobab.desktop.in:3
#: src/baobab-main-window.ui:120
msgid "Disk Usage Analyzer"
msgstr "ディスク使用量アナライザー"

#: data/org.gnome.baobab.appdata.xml.in:7 data/org.gnome.baobab.desktop.in:4
msgid "Check folder sizes and available disk space"
msgstr "フォルダーと利用可能なディスク容量をチェックします"

#: data/org.gnome.baobab.appdata.xml.in:9
msgid ""
"A simple application to keep your disk usage and available space under "
"control."
msgstr ""

#: data/org.gnome.baobab.appdata.xml.in:12
msgid ""
"Disk Usage Analyzer can scan specific folders, storage devices and online "
"accounts. It provides both a tree and a graphical representation showing the "
"size of each folder, making it easy to identify where disk space is wasted."
msgstr ""

#: data/org.gnome.baobab.appdata.xml.in:34
msgid "The GNOME Project"
msgstr "The GNOME Project"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.baobab.desktop.in:6
msgid "storage;space;cleanup;"
msgstr ""
"storage;space;cleanup;ストレージ;スペース;空間;容量;使用量;クリーンアップ;"

#: data/org.gnome.baobab.gschema.xml:9
msgid "Excluded partitions URIs"
msgstr "対象外となるパーティションの URI"

#: data/org.gnome.baobab.gschema.xml:10
msgid "A list of URIs for partitions to be excluded from scanning."
msgstr "スキャンを実行しないパーティションの URI を要素とするリストです。"

#: data/org.gnome.baobab.gschema.xml:20
msgid "Active Chart"
msgstr "アクティブなチャート"

#: data/org.gnome.baobab.gschema.xml:21
msgid "Which type of chart should be displayed."
msgstr "表示すべきチャートの種類を設定します。"

#: data/org.gnome.baobab.gschema.xml:25
msgid "Window size"
msgstr "ウィンドウのサイズ"

#: data/org.gnome.baobab.gschema.xml:26
msgid "The initial size of the window"
msgstr "ウィンドウの初期サイズです。"

#: data/org.gnome.baobab.gschema.xml:30
msgid "Window state"
msgstr "ウィンドウの状態"

#: data/org.gnome.baobab.gschema.xml:31
msgid "The GdkWindowState of the window"
msgstr "GdkWindowState で示される、ウィンドウの状態です。"

#: src/baobab-application.vala:30
msgid "Print version information and exit"
msgstr "バージョン情報を表示して終了する"

#: src/baobab-cellrenderers.vala:91
#, c-format
msgid "%d item"
msgid_plural "%d items"
msgstr[0] "%d 個のアイテム"

#. Translators: when the last modified time is unknown
#: src/baobab-cellrenderers.vala:101 src/baobab-location-list.vala:79
msgid "Unknown"
msgstr "不明"

#. Translators: when the last modified time is today
#: src/baobab-cellrenderers.vala:110
msgid "Today"
msgstr "今日"

#. Translators: when the last modified time is "days" days ago
#: src/baobab-cellrenderers.vala:114
#, c-format
msgid "%lu day"
msgid_plural "%lu days"
msgstr[0] "%lu 日前"

#. Translators: when the last modified time is "months" months ago
#: src/baobab-cellrenderers.vala:118
#, c-format
msgid "%lu month"
msgid_plural "%lu months"
msgstr[0] "%lu ヶ月前"

#. Translators: when the last modified time is "years" years ago
#: src/baobab-cellrenderers.vala:122
#, c-format
msgid "%lu year"
msgid_plural "%lu years"
msgstr[0] "%lu 年前"

#: src/baobab-location-list.ui:17
msgid "This Computer"
msgstr "このコンピューター"

#: src/baobab-location-list.ui:47
msgid "Remote Locations"
msgstr "リモートの場所"

#: src/baobab-location-list.vala:66
#, c-format
msgid "%s Total"
msgstr "合計 %s"

#: src/baobab-location-list.vala:70
#, c-format
msgid "%s Available"
msgstr "利用可能 %s"

#. useful for some remote mounts where we don't know the
#. size but do have a usage figure
#: src/baobab-location-list.vala:84
#, c-format
msgid "%s Used"
msgstr "使用済み %s"

#: src/baobab-location-list.vala:86
msgid "Unmounted"
msgstr "マウントされていません"

#: src/baobab-location.vala:73
msgid "Home folder"
msgstr "ホームフォルダー"

#: src/baobab-location.vala:113
msgid "Computer"
msgstr "コンピューター"

#: src/baobab-main-window.ui:7
msgid "Scan Folder…"
msgstr "フォルダーをスキャン…"

#: src/baobab-main-window.ui:13
msgid "Keyboard _Shortcuts"
msgstr "キーボードショートカット(_S)"

#: src/baobab-main-window.ui:17
msgid "_Help"
msgstr "ヘルプ(_H)"

#: src/baobab-main-window.ui:21
msgid "_About Disk Usage Analyzer"
msgstr "ディスク使用量アナライザーについて(_A)"

#: src/baobab-main-window.ui:34 src/menus.ui:7
msgid "_Open Folder"
msgstr "フォルダーを開く(_O)"

#: src/baobab-main-window.ui:43 src/menus.ui:11
msgid "_Copy Path to Clipboard"
msgstr "パスをクリップボードにコピーする(_C)"

#: src/baobab-main-window.ui:52 src/menus.ui:15
msgid "Mo_ve to Trash"
msgstr "ゴミ箱へ移動する(_V)"

#: src/baobab-main-window.ui:184
msgid "Close"
msgstr "閉じる"

#: src/baobab-main-window.ui:239
msgid "Folder"
msgstr "フォルダー"

#: src/baobab-main-window.ui:266
msgid "Size"
msgstr "サイズ"

#: src/baobab-main-window.ui:286
msgid "Contents"
msgstr "内容"

#: src/baobab-main-window.ui:304
msgid "Modified"
msgstr "更新日"

#: src/baobab-main-window.ui:349
msgid "Rings Chart"
msgstr "リングチャート"

#: src/baobab-main-window.ui:361
msgid "Treemap Chart"
msgstr "ツリーマップチャート"

#: src/baobab-window.vala:215
msgid "Select Folder"
msgstr "フォルダーの選択"

#: src/baobab-window.vala:217
msgid "_Cancel"
msgstr "キャンセル(_C)"

#: src/baobab-window.vala:218
msgid "_Open"
msgstr "開く(_O)"

#: src/baobab-window.vala:224
msgid "Recursively analyze mount points"
msgstr "マウントポイントを再帰的に解析"

#: src/baobab-window.vala:259
msgid "Could not analyze volume."
msgstr "ボリュームを解析できませんでした。"

#: src/baobab-window.vala:291
msgid "Failed to show help"
msgstr "ヘルプの表示に失敗しました"

#: src/baobab-window.vala:310
msgid "Baobab"
msgstr "Baobab"

#: src/baobab-window.vala:313
msgid "A graphical tool to analyze disk usage."
msgstr "ディスクの使用量を解析するグラフィカルなツールです。"

#: src/baobab-window.vala:318
msgid "translator-credits"
msgstr ""
"相花 毅 <takeshi.aihana@gmail.com>\n"
"草野 貴之 <AE5T-KSN@asahi-net.or.jp>\n"
"佐藤 暁 <ss@gnome.gr.jp>\n"
"高橋 宗史 <shuuji3@gmail.com>\n"
"日本 GNOME ユーザー会 http://www.gnome.gr.jp/\n"
"松澤 二郎 <jmatsuzawa@gnome.org>\n"
"やまねひでき <henrich@debian.or.jp>\n"
"Eiichiro ITANI <emu@ceres.dti.ne.jp>\n"
"KAMAGASAKO Masatoshi <emerald@gnome.gr.jp>\n"
"Noriko Mizumoto <noriko@fedoraproject.org>\n"
"sicklylife <translation@sicklylife.jp>\n"
"Sun G11n <gnome_int_l10n@ireland.sun.com>\n"
"Takayoshi OKANO <kano@na.rim.or.jp>\n"
"Yasumichi Akahoshi <yasumichi@vinelinux.org>\n"
"Yukihiro Nakai <nakai@gnome.gr.jp>\n"
"Yuusuke Tahara <tahara@gnome.gr.jp>"

#: src/baobab-window.vala:387
msgid "Failed to open file"
msgstr "ファイルを開くのに失敗しました"

#: src/baobab-window.vala:407
msgid "Failed to move file to the trash"
msgstr "ファイルをゴミ箱に移動するのに失敗しました"

#: src/baobab-window.vala:511
msgid "Devices & Locations"
msgstr "デバイスと場所"

#: src/baobab-window.vala:573
#, c-format
msgid "Could not scan folder “%s”"
msgstr "フォルダー“%s”をスキャンできませんでした"

#: src/baobab-window.vala:576
#, c-format
msgid "Could not scan some of the folders contained in “%s”"
msgstr "“%s”配下にあるフォルダーのいくつかをスキャンできませんでした"

#: src/baobab-window.vala:595
msgid "Could not detect occupied disk sizes."
msgstr "割り当てディスクサイズを検出できませんでした。"

#: src/baobab-window.vala:595
msgid "Apparent sizes are shown instead."
msgstr "実サイズを代わりに表示します。"

#: src/baobab-window.vala:599
msgid "Scan completed"
msgstr "スキャン完了"

#: src/baobab-window.vala:600
#, c-format
msgid "Completed scan of “%s”"
msgstr "“%s”のスキャンを完了しました"

#. || is_virtual_filesystem ()
#: src/baobab-window.vala:618 src/baobab-window.vala:624
#, c-format
msgid "“%s” is not a valid folder"
msgstr "“%s”は正常なフォルダーではありません"

#: src/baobab-window.vala:619 src/baobab-window.vala:625
msgid "Could not analyze disk usage."
msgstr "ディスクの使用量を解析できませんでした。"

#: src/help-overlay.ui:13
msgctxt "shortcut window"
msgid "General"
msgstr "全般"

#: src/help-overlay.ui:18
msgctxt "shortcut window"
msgid "Show / Hide primary menu"
msgstr "プライマリメニューの表示/非表示を切り替える"

#: src/help-overlay.ui:25
msgctxt "shortcut window"
msgid "Rescan current location"
msgstr "現在の場所を再スキャンする"

#: src/help-overlay.ui:32
msgctxt "shortcut window"
msgid "Scan folder"
msgstr "フォルダーをスキャンする"

#: src/help-overlay.ui:38
msgctxt "shortcut window"
msgid "Show Keyboard Shortcuts"
msgstr "キーボードショートカットを表示する"

#: src/help-overlay.ui:46
msgctxt "shortcut window"
msgid "Go back to location list"
msgstr "場所の一覧に戻る"

#: src/help-overlay.ui:52
msgctxt "shortcut window"
msgid "Quit"
msgstr "終了する"

#: src/menus.ui:21
msgid "Go to _parent folder"
msgstr "親フォルダーへ移動する(_P)"

#: src/menus.ui:27
msgid "Zoom _in"
msgstr "拡大する(_I)"

#: src/menus.ui:31
msgid "Zoom _out"
msgstr "縮小する(_O)"

#~ msgid "org.gnome.baobab"
#~ msgstr "org.gnome.baobab"

#~ msgid ""
#~ "A simple application which can scan either specific folders (local or "
#~ "remote) or volumes and give a graphical representation including each "
#~ "directory size or percentage."
#~ msgstr ""
#~ "フォルダー (ローカルまたはリモート) あるいはボリュームをスキャンして、各"
#~ "ディレクトリのサイズや使用率をグラフィカルに表示できるシンプルなアプリケー"
#~ "ションです。"

#~ msgid "Scan Remote Folder…"
#~ msgstr "リモートフォルダーのスキャン…"

#~ msgid "_About"
#~ msgstr "このアプリケーションについて(_A)"
